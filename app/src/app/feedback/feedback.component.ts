import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.less'],
})
export class FeedbackComponent implements OnInit {
  FeedbackForm: FormGroup;
  user: any = {};
  submitted = false;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.FeedbackForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      number: ['', Validators.required],
      content: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  get f() {
    return this.FeedbackForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.FeedbackForm.invalid) {
      return;
    }
  }
  save() {
    console.log(this.FeedbackForm.value);
    this.user = Object.assign(this.user, this.FeedbackForm.value);
    localStorage.setItem('users', JSON.stringify(this.user));
    alert('thanks for feedback');
  }
}

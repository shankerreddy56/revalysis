﻿import { Component } from '@angular/core';

import { User } from '@app/_models';
import { AccountService } from '@app/_services';
import { sampleDataService } from '@app/_services/sampleData.service';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
  user: User;
  result: any;
  constructor(
    private service: sampleDataService,
    private accountService: AccountService
  ) {
    this.user = this.accountService.userValue;
  }
  ngOnInit() {
    this.service.getData().subscribe((posres) => {
      this.result = posres;
    });
  }
  onScroll() {
    console.log('scrolled');
  }
  key: string = 'Price';
  reverse: boolean = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
}
